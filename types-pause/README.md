# Installation
> `npm install --save @types/pause`

# Summary
This package contains type definitions for pause (https://github.com/stream-utils/pause).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/pause.
## [index.d.ts](https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/pause/index.d.ts)
````ts
/// <reference types="node" />
import * as stream from "stream";

export = pause;

declare function pause(stream: stream.Stream): pause.Handle;

declare namespace pause {
    interface Handle {
        end(): void;
        resume(): void;
    }
}

````

### Additional Details
 * Last updated: Tue, 07 Nov 2023 09:09:39 GMT
 * Dependencies: [@types/node](https://npmjs.com/package/@types/node)

# Credits
These definitions were written by [BendingBender](https://github.com/BendingBender).
